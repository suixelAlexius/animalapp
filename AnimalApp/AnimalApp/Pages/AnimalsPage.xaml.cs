﻿using AnimalApp.Data;
using AnimalApp.MyTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AnimalApp.Pages
{
    /// <summary>
    /// Interaction logic for AnimalsPage.xaml
    /// </summary>
    public partial class AnimalsPage : Page
    {
        public AnimalsPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindow.DB = new AnimalDBEntities();
            CBType.ItemsSource = Tools.CreateComboBoxList(MainWindow.DB.Type.ToList(), new Data.Type() { Name = "Все" });
            CBBreed.ItemsSource = Tools.CreateComboBoxList(MainWindow.DB.Breed.ToList(), new Breed() { Name = "Все" });
            CBType.SelectedIndex = 0;
            CBBreed.SelectedIndex = 0;
            Refresh();
        }

        private void Refresh()
        {
            var filteredAnimals = MainWindow.DB.Animal.ToList();
            if (!string.IsNullOrWhiteSpace(TBSearch.Text))
            {
                var search = TBSearch.Text.ToLower();
                filteredAnimals = filteredAnimals.Where(i => i.Name.ToLower().Contains(search)).ToList();
            }
            if (CBType.SelectedIndex > 0)
            {
                var selectedType = CBType.SelectedItem as Data.Type;
                filteredAnimals = filteredAnimals.Where(i => i.Breed.TypeId == selectedType.Id).ToList();
            }

            if (CBBreed.SelectedIndex > 0)
            {
                var selectedBreed = CBBreed.SelectedItem as Breed;
                filteredAnimals = filteredAnimals.Where(i => i.BreedId == selectedBreed.Id).ToList();
            }

            if (CBDate.IsChecked == true)
            {
                var dateFrom = DPFrom.SelectedDate;
                if (dateFrom != null)
                    filteredAnimals = filteredAnimals.Where(i => i.DateOfBirth >= dateFrom).ToList();
                var dateTo = DPTo.SelectedDate;
                if (dateTo != null)
                    filteredAnimals = filteredAnimals.Where(i => i.DateOfBirth <= dateTo).ToList();
            }

            DGAnimals.ItemsSource = filteredAnimals;
        }

        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new AnimalPage(
                new Animal()
                { DateOfBirth = DateTime.Today }
                ));
        }

        private void BEdit_Click(object sender, RoutedEventArgs e)
        {
            var selectedAnimal = DGAnimals.SelectedItem as Animal;
            if (selectedAnimal == null)
                return;
            NavigationService.Navigate(new AnimalPage(selectedAnimal));
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            var selectedAnimal = DGAnimals.SelectedItem as Animal;
            if (selectedAnimal == null)
                return;
            MainWindow.DB.Animal.Remove(selectedAnimal);
            MainWindow.DB.SaveChanges();
            Refresh();
        }

        private void TBSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            Refresh();
        }

        private void CBType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Refresh();
        }

        private void CBDate_Checked(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void CBDate_Unchecked(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void DPFrom_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            Refresh();
        }

        private void DPTo_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            Refresh();
        }

        private void BClear_Click(object sender, RoutedEventArgs e)
        {
            CBBreed.SelectedIndex = 0;
            CBType.SelectedIndex = 0;
            CBDate.IsChecked = false;
            TBSearch.Text = "";
        }
    }
}
