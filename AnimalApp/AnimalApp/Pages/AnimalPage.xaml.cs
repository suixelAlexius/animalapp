﻿using AnimalApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AnimalApp.Pages
{
    /// <summary>
    /// Interaction logic for AnimalPage.xaml
    /// </summary>
    public partial class AnimalPage : Page
    {
        Animal contextAnimal;
        public AnimalPage(Animal postAnimal)
        {
            InitializeComponent();
            CBBreed.ItemsSource = MainWindow.DB.Breed.ToList();
            contextAnimal = postAnimal;
            this.DataContext = contextAnimal;
        }

        private void BSave_Click(object sender, RoutedEventArgs e)
        {
            string message = "";
            if (string.IsNullOrWhiteSpace(contextAnimal.Name))
                message += "Введите имя! \n\r";
            if (contextAnimal.Breed == null)
                message += "Выберите породу\n\r";
            if(message != "")
            {
                MessageBox.Show(message);
                return;
            }

            //id = 0 только у новых животных, не добавленных в БД
            if (contextAnimal.Id == 0)
                MainWindow.DB.Animal.Add(contextAnimal);
            MainWindow.DB.SaveChanges();
            NavigationService.GoBack();
        }

        private void BBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
