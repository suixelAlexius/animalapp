﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalApp.MyTools
{
    public static class Tools
    {
        public static List<T> CreateComboBoxList<T>(List<T> source, T item)
        {
            var result = source.ToList();
            result.Insert(0, item);
            return result;
        }
    }
}
